import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import combinedReducers from '../reducers/combineReducers'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas'

export default function configureStore ( initialState ){
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore( combinedReducers, 
        //initialState,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        applyMiddleware(sagaMiddleware) )

    store.runSaga = sagaMiddleware.run(rootSaga)
    return store
}
