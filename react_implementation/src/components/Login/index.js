import React from 'react';
import './index.css';
import logo from '../../logo.svg';

const Login = ({onSubmitLoginForm, onInputChange, username, password, isFetching}) => {
      return (
        <form className="form-signin" onSubmit={onSubmitLoginForm}>
            <img className="mb-4" src={ logo } alt="" width="72" height="72" />
            <h1 className="h3 mb-3 font-weight-normal">Inicio de Sesión</h1>
            <label htmlFor="username" className="sr-only">Email address</label>
                <input type="text" id="username" name="username" className="form-control" placeholder="Nombre de Usuario" onChange={onInputChange} value={username} required autoFocus />
            <label htmlFor="password" className="sr-only">Password</label>
                <input type="password" id="password" name="password" className="form-control" placeholder="Contraseña" onChange={onInputChange} value={password} required />
            <button className="btn btn-lg btn-primary btn-block" type="submit">Iniciar</button>
            <p className="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
        </form>
      )
  }
  
  export default Login;