import React from 'react'
import Header from '../../Header'
import Sidebar from '../../Sidebar'

const MainTheme = ( { children } ) => {
    return(
        <div>
            <Header />
            <Sidebar />
            { children }
        </div>
    )
}

export default MainTheme