import React from 'react'
import FeatherIcon from 'feather-icons-react'
import { Link } from 'react-router-dom';

const GridTable = ( props ) => {

    //const self=this
    const { List } = props

    return (
        <div className="table-responsive">
            <table className="table table-striped table-sm">
              <thead>
                <tr>
                    {
                        props.Headers.map((column_name,  index ) => (
                            <th key={ index }>{column_name}</th>
                        ))
                    }
                </tr>
              </thead>
              <tbody>
                {
                    List.map((item, index) => (
                        <tr key={index}>
                            <td>{parseInt(index+1,10)}</td>
                            <td>{item.name}</td>
                            <td>{item.lastname}</td>
                            <td>{item.username}</td>
                            <td>
                                <Link className="" to={`/users/show/${item._id}`}><FeatherIcon icon='eye' size="18" />  </Link>
                                <Link className="" to={`/users/edit/${item._id}`}><FeatherIcon icon='edit' size="18" />  </Link>
                                <Link className="" to={`/users/delete/${item._id}`}><FeatherIcon icon='trash-2' size="18" />  </Link>
                            </td>
                        </tr>
                    ))
                }
              </tbody>
            </table>
          </div>
    )
}

export default GridTable