import React from 'react'
import FeatherIcon from 'feather-icons-react'
import { Link } from 'react-router-dom'

const Title = ( { title, icon, route } ) => {

    let MyIcon = '';
    
    if(icon){
        MyIcon = (<Link to={route}><FeatherIcon icon={icon} size="18" /></Link>)
    }
    
    return (
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 className="h2">{title} {MyIcon}</h1>
        </div>
    )
}

export default Title