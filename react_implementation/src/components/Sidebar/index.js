import React from 'react';
import { Link } from 'react-router-dom';
import './index.css';
import FeatherIcon from 'feather-icons-react';

const MenuList = [
  { id: 1, key: 1, name: 'Inicio', route:'/home', icon: 'home', link: '' },
  { id: 2, key: 2, name: 'Usuarios', route:'/users', icon: 'users', link: '' }
]

const Sidebar = () => {
      return (
        <nav className="col-md-2 d-none d-md-block bg-light sidebar text-left">
          <div className="sidebar-sticky">
            <ul className="nav flex-column">
              {
                MenuList.map((item,  index ) => (
                  <li className="nav-item" key={index}>
                    <Link className="nav-link" to={item.route}><FeatherIcon icon={item.icon} size="18" />
                      {item.name}
                    </Link>
                  </li>
                ))
              }
            </ul>
          </div>
        </nav>
      )
  }
  
  export default Sidebar;