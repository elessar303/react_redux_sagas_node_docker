import React from 'react';
import { Link } from 'react-router-dom';
import './index.css';

const Header = () => {
      return (
        <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <Link className="navbar-brand col-sm-3 col-md-2 mr-0 text-left" to='/home'>Mi Compañia</Link>
            <input className="form-control form-control-dark w-100" type="text" placeholder="Buscar" aria-label="Buscar" />
            <ul className="navbar-nav px-3">
                <li className="nav-item text-nowrap">
                    <Link className="nav-link" to='/'>Salir</Link>
                </li>
            </ul>
        </nav>
      )
  }
  
  export default Header;