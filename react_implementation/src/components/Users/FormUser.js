import React from 'react'
import './FormUser.css'

const FormUser = ( {onSubmitUserForm, onInputChangeValue, data, showPrimaryButton = true, showDeleteButton = false } ) => {

    let buttonControlPrimary = null
    if(showPrimaryButton===true){

        buttonControlPrimary = (
        <div className="form-row justify-content-end">
            <button type="submit" className="btn btn-primary">Guardar</button>
        </div>)
    }

    let buttonControlDelete = null
    if(showDeleteButton===true){

        buttonControlDelete = (
        <div className="form-row justify-content-end">
            <button type="submit" className="btn btn-danger">Eliminar</button>
        </div>)
    }

    return(
        <form onSubmit={onSubmitUserForm}>
            <div className="form-row">
                <div className="form-group col-md-4">
                    <label htmlFor="username">Usuario</label>
                    <input type="text" className="form-control" id="username" name="username" placeholder="Usuario" onChange={onInputChangeValue} value={data.username || ''} />
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="name">Nombre</label>
                    <input type="text" className="form-control" id="name" name="name" placeholder="Nombre" onChange={onInputChangeValue} value={data.name || ''} />
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="lastname">Apellido</label>
                    <input type="text" className="form-control" id="lastname" name="lastname" placeholder="Apellido" onChange={onInputChangeValue} value={data.lastname || ''} />
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-md-4">
                    <label htmlFor="state">Estado</label>
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="password">Clave</label>
                    <input type="text" className="form-control" id="password" name="password" placeholder="Clave" onChange={onInputChangeValue} value={data.password || ''} />
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="password1">Repetir clave</label>
                    <input type="text" className="form-control" id="password1" name="password1" placeholder="Repetir clave" onChange={onInputChangeValue}  value={data.password1 || ''}/>
                </div>
            </div>
            {buttonControlPrimary}
            {buttonControlDelete}
        </form>
    )

}

export default FormUser