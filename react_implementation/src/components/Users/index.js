import React from 'react'
import GridTable from '../Elements/GridTable'

const Headers = ['Nº', 'Nombre', 'Apellido', 'Usuario', 'Accion']

const Users = ( {data, editUser, deleteUser} ) => {
    return(
        <GridTable Headers={Headers} List={data} deleteUser={deleteUser} editUser={editUser}/>
    )

}

export default Users