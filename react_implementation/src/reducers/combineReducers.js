import { combineReducers } from 'redux'
import users from './usersReducers'
import auth from './authReducers'

const combinedReducers = combineReducers({
    users,
    auth
})

export default combinedReducers