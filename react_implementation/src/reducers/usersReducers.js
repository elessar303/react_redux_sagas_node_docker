import { createReducer } from 'redux-act'
import { combineReducers } from 'redux'
import * as UserActions from '../actions/UserActions'

const initial = {
    Add:{
        beginTime :'',
        error: {},
        current: {},
        isFetching: false,
        Success: false
    },
    Show:{
        beginTime :'',
        error: {},
        current: {},
        isFetching: false
    },
    Edit:{
        beginTime :'',
        error: {},
        current: {},
        isFetching: false,
        Success: false
    },
    Remove:{
        beginTime :'',
        error: {},
        current: {},
        isFetching: false
    },
    List:{
        data: [],
        error: {},
        isFetching: false
    }
}

const Add = createReducer({

    [UserActions.add]:  (state, payload) =>{

        return {
            ...state
        }
    },
    [UserActions.addBeginRequest]:  (state, payload) =>{

        return {
            ...state,
            isFetching : true,
            //Success : false
        }
    },
    [UserActions.addResponseSuccesfull]:  (state, payload) =>{

        return {
            ...state,
            isFetching : false,
            //Success : true
        }
    }
    
}, initial.Add)

const Show = createReducer({

    [UserActions.show]:  (state, payload) =>{
        return {
            ...state,
            current:{}

        }
    },
    [UserActions.updateColumn]:  (state, payload) =>{
        const current = state.current

        return {
            ...state,
            current:{...current, [payload.columnName] : payload.value}
        }
    },
    [UserActions.showBeginRequest]:  (state, payload) =>{
        return {
            current:{}

        }
    },
    [UserActions.showResponseSuccesfull]:  (state, payload) =>{
        
        return {
            ...state,
            current: payload
        }
    },
    [UserActions.showResetCurrentUser]:  (state, payload) =>{
        return {
            current:{}

        }
    },
}, initial.Show)

const Edit = createReducer({

    [UserActions.edit]:  (state, payload) =>{

        return {
            ...state,
            current:{},
            isFetching : true,
            Success : true

        }
    },
    [UserActions.editResponseSuccesfull]:  (state, payload) =>{

        return {
            ...state,
            isFetching : false,
        }
    }
}, initial.Edit)

const Remove = createReducer({

    [UserActions.remove]:  (state, payload) =>{
        
        return {
            ...state,
            current:{},
            isFetching : true,

        }
    }
}, initial.Remove)

const List = createReducer({

    [UserActions.list]:  (state, payload) =>{
        return {
            ...state,
            isFetching : true
        }
    },
    [UserActions.listResponseSuccesfull]:  (state, payload) =>{
        return {
            ...state,
            isFetching : false,
            data : payload
        }
    }
}, initial.List)

export default combineReducers({
    Add,
    Edit,
    Remove,
    Show,
    List
})
