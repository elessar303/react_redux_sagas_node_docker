import { createReducer } from 'redux-act'
import { combineReducers } from 'redux'
import * as AuthActions from '../actions/AuthActions'

const initial = {
    Login:{
        error: {},
        current: {},
        isFetching: false,
        Success: false
    }
}

const Login = createReducer({

    [AuthActions.login]:  (state, payload) =>{
        return {
            ...state,
            isFetching: true,
        }
    },
    
}, initial.Login)

export default combineReducers({
    Login
})