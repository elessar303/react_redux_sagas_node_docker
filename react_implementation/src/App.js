import React, { Component } from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import io from 'socket.io-client'
import configureStore from './store/configureStore'
import history from './utils/history'
import LoginPage from './containers/LoginPage'
import HomePage from './containers/HomePage'
import UsersPage from './containers/UsersPage'
import UserAddPage from './containers/UserAddPage'
import UserShowPage from './containers/UserShowPage'
import UserEditPage from './containers/UserEditPage'
import UserDeletePage from './containers/UserDeletePage'
import MainTemplate from './containers/MainTemplate'

import { Widget } from 'react-chat-widget';
 
import 'react-chat-widget/lib/styles.css';

const store = configureStore()

var socket = io('http://173.18.0.22')

socket.emit('holaServer', {name: 'Soy el cliente'})

socket.on('asignarCedula', function(data){
  //console.log(data)
})

socket.on('msjServer', function(data){
  //console.log(data)
})

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Switch>
          <Route path='/' exact component={ LoginPage }/>
          <MainTemplate>
            <div className="container-fluid">
              <div className="row">
                <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                  <Route path='/home' exact component={ HomePage }/>
                  <Route path='/users' exact component={ UsersPage }/>
                  <Route path='/users/add' exact component={ UserAddPage }/>
                  <Route path='/users/show/:id' exact component={ UserShowPage }/>
                  <Route path='/users/edit/:id' exact component={ UserEditPage }/>
                  <Route path='/users/delete/:id' exact component={ UserDeletePage }/>
                </main>
              </div>
            </div>
            <Widget />
          </MainTemplate>
          </Switch>
        </Router>
      </Provider>
    );
  }
}
