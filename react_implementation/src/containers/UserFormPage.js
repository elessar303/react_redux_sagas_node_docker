import React, { Component } from 'react'
import FormUser from '../components/Users/FormUser'
import PropTypes from 'prop-types';


class UserFormPage extends Component {

    onSubmitUserForm = (e) => {
        e.preventDefault()
        this.props.onSubmitForm(this.props.data)
    }

    onInputChangeValue = (e) => {
        const columnName = e.target.name
        const value = e.target.value
        this.props.onInputChange(columnName, value)
    }

    render(){
        return (<FormUser 
            {...this.props}
            data={this.props.data} 
            showPrimaryButton={this.props.showPrimaryButton} 
            showDeleteButton={this.props.showDeleteButton} 
            onSubmitUserForm={this.onSubmitUserForm} 
            onInputChangeValue={this.onInputChangeValue} />)
    }
}

UserFormPage.propTypes = {
    lastname: PropTypes.string,
    onInputChangeValue: PropTypes.func.isRequired

}

UserFormPage.defaultProps = {
    onInputChangeValue:()=>{},
    data:{
        username:''},
        lastname: '',
        name:'',
        password:'',
        password1:''
    
}

export default UserFormPage