import React, { Component } from 'react'
import HomePageComponent from '../components/Home'

class HomePage extends Component {

    render() {
        return(
            <HomePageComponent />
        )
    }
}

export default HomePage