import React, { Component } from 'react'
import Template from '../components/Layouts/MainTheme'

class TemplatePage extends Component {

    render() {
        return(
            <Template>
            {this.props.children}
            </Template>
        )
    }
}

export default TemplatePage