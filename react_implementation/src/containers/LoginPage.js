import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import Login from '../components/Login'
import * as AuthActions from '../actions/AuthActions'


class LoginPage extends Component {

    constructor( props ){

        super( props )

        this.state={
               username:'',
               password:''
        }
    }

    onSubmitLoginForm = (e) => {
        e.preventDefault()
        const data = this.state;
        this.props.actions.login(data)
    }

    onInputChangeValue = ( e ) => {
        this.setState({ ...this.state, [e.target.name]: e.target.value });
    }

    render() {
        return(
            <Login onSubmitLoginForm={this.onSubmitLoginForm} onInputChange={this.onInputChangeValue} username={this.state.username} password={this.state.password} isFetching={this.props.isFetching}/>
        )
    }
}

LoginPage.propTypes = {
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired

}

LoginPage.defaultProps = {
    username:'',
    password:''
}

const mapStateToProps = ( state ) => {
    return{
        isFetching: state.auth.Login.isFetching
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        actions: bindActionCreators(AuthActions, dispatch)        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)