import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import PageTitle from '../components/Elements/PageTitle'
import FormUser from '../containers/UserFormPage'
import * as UserActions from '../actions/UserActions'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'

class UserEditPage extends Component {

    onSubmitForm = () => {

        const id = this.props.match.params.id
        confirmAlert({
            title: 'Eliminar',
            message: 'Se eliminara este registro',
            buttons: [
              {
                label: 'Si',
                onClick: () => this.props.actions.remove(id)
              },
              {
                label: 'No',
                onClick: () => false
              }
            ]
        })
    }

    onInputChangeValue = (columnName, value) => {
        return false
    }

    componentDidMount(){
        this.props.actions.show(this.props.match.params.id)
    }

    componentWillUnmount(){
        this.props.actions.showResetCurrentUser()
    }

    render(){

        return(
            <div>
                <PageTitle title='Eliminar Usuario'/>
                <FormUser showPrimaryButton={false} showDeleteButton={true} data={this.props.data} onSubmitForm= {this.onSubmitForm} onInputChange= {this.onInputChangeValue} />
            </div>
        )

    }
}

const mapStateToProps = ( state ) => {
    return{
        data: state.users.Show.current
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        actions: bindActionCreators(UserActions, dispatch)        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEditPage)