import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import PageTitle from '../components/Elements/PageTitle'
import FormUser from '../containers/UserFormPage'
import * as UserActions from '../actions/UserActions'

class UserEditPage extends Component {

    onSubmitForm = (data) => {
        this.props.actions.edit(data)
    }

    onInputChangeValue = (columnName, value) => {
        this.props.actions.updateColumn({columnName, value})
    }

    componentDidMount(){        
        this.props.actions.show(this.props.match.params.id)
    }

    componentWillUnmount(){
        this.props.actions.showResetCurrentUser()
    }


    render(){

        return(
            <div>
                <PageTitle title='Editar Usuario'/>
                <FormUser data={this.props.data} onSubmitForm= {this.onSubmitForm} onInputChange= {this.onInputChangeValue} />
            </div>
        )

    }
}

const mapStateToProps = ( state ) => {
    return{
        data: state.users.Show.current
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        actions: bindActionCreators(UserActions, dispatch)        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEditPage)