import React, { Component } from 'react'
import { connect } from 'react-redux'
import PageTitle from '../components/Elements/PageTitle'
import Users from '../components/Users'
import * as UserActions from '../actions/UserActions'
import Loader from '../components/Elements/Spinners'

class UsersPage extends Component {

    componentDidMount(){
        this.props.LoadUsersContainer()
    }

    deleteUser = (e,id) => {
        e.preventDefault()
        this.props.deleteUserContainer(id)
    }

    editUser = (e,id) => {
        e.preventDefault()
        this.props.editUserContainer(id)
    }

    render(){
        const { UserList, Loading } = this.props
        let body = ''

        if(!Loading){
            body = <Users data={UserList} deleteUser={this.deleteUser} editUser={this.editUser}/>
        }else{
            body = <Loader />
        }
        return(
            <div>
                <PageTitle title='Usuarios' icon='plus-circle' route='/users/add'/>
                {body}
            </div>
        )

    }
}

const mapStateToProps = ( state ) =>{
    
    const UserData = state.users.List.data || []
    const isFetching = state.users.List.isFetching
    return {
        UserList: UserData,
        Loading: isFetching

    }
}

const mapDispatchToProps = ( dispatch ) =>{

    return {
        
        LoadUsersContainer(){
            dispatch(UserActions.list())
        },
        deleteUserContainer(id){
            dispatch(UserActions.remove(id))
        },
        editUserContainer(id){
            dispatch(UserActions.edit(id))
        }
    }         
    
}
export default connect(mapStateToProps,mapDispatchToProps)(UsersPage)