import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import {Redirect } from 'react-router-dom'
import PageTitle from '../components/Elements/PageTitle'
import FormUser from '../containers/UserFormPage'
import Loader from '../components/Elements/Spinners'
import * as UserActions from '../actions/UserActions'
import PropTypes from 'prop-types';


class UserAddPage extends Component {

    onSubmitUserForm = () => {
        const formData = this.props.data
        this.props.actions.addBeginRequest(formData)
    }

    onInputChange = (columnName, value) => {
        this.props.actions.updateColumn({columnName, value})
    }

    componentDidMount(){
        this.props.actions.add()
    }

    render(){

        const { Loading, Success } = this.props
        let body = ''
        if(Success===true && Loading === false){
            return (<Redirect to="/users"/>)
        }
        if(!Loading){
            body = <FormUser data={this.props.data} onSubmitForm={this.onSubmitUserForm} onInputChange={this.onInputChange} />
        }else{
            body = <Loader />
        }
        return(
            <div>
                <PageTitle title='Agregar Usuario'/>
                {body}
            </div>
        )

    }
}

const mapStateToProps = ( state ) => {

    return{
        beginTime: state.users.Add.beginTime,
        data: state.users.Show.current,
        Loading : state.users.Add.isFetching,
        Success: state.users.Add.Success
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        actions:bindActionCreators(UserActions, dispatch)
    }
}

UserAddPage.propTypes = {
    lastname: PropTypes.string,
    onInputChange: PropTypes.func.isRequired

}

UserAddPage.defaultProps = {
    onInputChange:()=>{},
    data:{username:''},
    lastname: '',
    name:'',
    password:'',
    password1:''
}


export default connect(mapStateToProps, mapDispatchToProps)(UserAddPage)