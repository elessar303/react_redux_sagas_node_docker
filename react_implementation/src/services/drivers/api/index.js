import axios from 'axios'

const request = axios.create({
    //baseURL: process.env.REACT_APP_API_URL
    baseURL: 'http://173.18.0.22/api'
})

export const list = () => {
    return new Promise((resolve, reject) => {
        request.get('/users')
        .then(function(response) {
            resolve(response.data)
        })
    })
}

export const show = (id) => {
    return new Promise((resolve, reject) => {
        request.get(`/users/${id}`)
        .then(function(response) {
            resolve(response.data)
        })
    })
}

export const remove = (id) => {
    return new Promise((resolve, reject) => {
        request.delete(`/users/${id}`)
        .then(function(response) {
            resolve(response.data)
        })
    })
}

export const add = (data) => {
    return new Promise((resolve, reject) => {
        request.post('/users', data)
        .then(function(response) {
            resolve(response.data)
        })
    })
}

export const edit = (id, data) => {
    return new Promise((resolve, reject) => {
        request.post(`/users/${id}`, data)
        .then(function(response) {
            resolve(response.data)
        })
    })
}