import { fork, call, put, take } from 'redux-saga/effects'
import history from '../utils/history'
import * as UserActions from '../actions/UserActions'
import * as apiService from '../services/drivers/api'

function* add(){
    while( true ){
        try{
            const { payload : dataForm} = yield take(UserActions.addBeginRequest)
            const HttpResponse = yield call(apiService.add, dataForm)
            yield put(UserActions.addResponseSuccesfull(HttpResponse))
            yield call(history.push, '/users');
        }
        catch( e ){
            console.log('Error:'+e)
        }
    }
}

function* edit(){
    while( true ){
        try{
            const { payload : data } = yield take(UserActions.edit)
            const id = data._id
            const HttpResponse = yield call(apiService.edit, id, data)
            yield put(UserActions.editResponseSuccesfull(HttpResponse))
            yield call(history.push, '/users');
                      
        }
        catch( e ){
            console.log('Error:'+e)
        }
    }
}

function* list(){
    while( true ){
        try{
            yield take(UserActions.list)
            const HttpResponse = yield call(apiService.list)
            yield put(UserActions.listBeginRequest(HttpResponse))
            yield put(UserActions.listResponseSuccesfull(HttpResponse))
        }
        catch( e ){
            yield put(UserActions.listResponseError(e))
        }
    }
}

function* show(){
    while( true ){
        try{
            const { payload: id } = yield take(UserActions.show)
            yield put(UserActions.showBeginRequest())
            const HttpResponse = yield call(apiService.show, id)
            yield put(UserActions.showResponseSuccesfull(HttpResponse[0]))
        }
        catch( e ){
            
            yield put(UserActions.showResponseError(e))
        }
    }
}

function* remove(){
    while( true ){
        try{
            const { payload: id } = yield take(UserActions.remove)
            yield put(UserActions.remove())
            const HttpResponse = yield call(apiService.remove, id)
            yield put(UserActions.removeResponseSuccesfull(HttpResponse[0]))
            yield call(history.push, '/users');
        }
        catch( e ){
            
            yield put(UserActions.showResponseError(e))
        }
    }
}

export  default function* rootSaga(){
    yield fork (add)
    yield fork (list)
    yield fork (edit)
    yield fork (show)
    yield fork (remove)
    console.log('saga start')

}