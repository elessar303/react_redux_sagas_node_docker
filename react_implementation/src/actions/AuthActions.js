import { createAction } from 'redux-act'

export const login = createAction("Form Login Submit")
export const loginResponseSuccesfull = createAction("Login Response Succesfull")
export const loginResponseError = createAction("Login Response Error")
