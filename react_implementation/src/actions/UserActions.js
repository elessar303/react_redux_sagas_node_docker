import { createAction } from 'redux-act'

export const add = createAction("Add User")
export const addBeginRequest = createAction("Add User Form Data")
export const addResponseSuccesfull = createAction("Add User Response Succesfull")
export const addResponseError = createAction("Add User Response Error")


export const edit = createAction("Edit User")
export const editBeginRequest = createAction("Edit User Begin Request")
export const editResponseSuccesfull = createAction("Edit User Response Succesfull")
export const editResponseError = createAction("Edit User Response Error")

export const remove = createAction("Remove User")
export const removeBeginRequest = createAction("Remove User Begin Request")
export const removeResponseSuccesfull = createAction("Remove User Response Succesfull")
export const removeResponseError = createAction("Remove User Response Error")

export const show = createAction("Show User")
export const showBeginRequest = createAction("Show User Begin Request")
export const showResponseSuccesfull = createAction("Show User Response Succesfull")
export const showResponseError = createAction("Show User Response Error")
export const showResetCurrentUser = createAction("Show Current User Data")


export const list = createAction("List User")
export const listBeginRequest = createAction("List User Begin Request")
export const listResponseSuccesfull = createAction("List User Response Succesfull")
export const listResponseError = createAction("List User Response Error")



export const updateColumn = createAction("Update User Input Value")