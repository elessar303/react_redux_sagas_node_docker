const express = require ('express')
const App = express()
const Router = express.Router()
const Server = require('http').Server(App)
const io = require('socket.io')(Server)
const cors = require('cors')
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const assert = require('assert');
const clients = []

// Connection URL
const url = 'mongodb://173.18.0.24:27017';

// Database Name
const dbName = 'learn_react';

App.use(cors())
App.use(bodyParser.json());
// Use connect method to connect to the server

Router.get('/users', function(request, response){

    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);    
        const db = client.db(dbName);
        const query = 'users'
        getDocuments(db,query,{}, function(users) {
            response.json(users)
            client.close();
          });
    });
})

Router.post('/users', function(request, response){

    const newData = request.body;
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);    
        const db = client.db(dbName);
        const query = 'users'
        delete newData['_id']
        insertDocuments(db, query, newData, function(insertResponse) {
            response.json(insertResponse)
            client.close();
        });
    });
})

Router.post('/users/:id', function(request, response){
    const newData = request.body;
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);    
        const db = client.db(dbName);
        const query = 'users'
        const id = request.params.id
        delete newData['_id']
        updateDocumentsById(db, query, id, newData, function(updatetResponse) {
            response.json(updatetResponse)
            client.close();
        });
    });
})

Router.delete('/users/:id', function(request, response){
    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);    
        const db = client.db(dbName);
        const query = 'users'
        const id = request.params.id
        deleteDocumentsById(db, query, id, function(updatetResponse) {
            response.json(updatetResponse)
            client.close();
        });
    });
})

Router.get('/users/:id', function(request, response){

    MongoClient.connect(url, function(err, client) {
        assert.equal(null, err);    
        const db = client.db(dbName);
        const query = 'users'
        const id = request.params.id
        getDocumentsById(db, query, id, function(user) {
            response.json(user)
            client.close();
          });
    });
})

const getDocuments = function(db, query, filter, callback) {
    // Get the documents collection
    const collection = db.collection(query);
    // Find some documents
    collection.find(filter).toArray(function(err, docs) {
      assert.equal(err, null);
      callback(docs);
    });
}

const getDocumentsById = function(db, query, id, callback) {
    // Get the documents collection
    const collection = db.collection(query);
    // Find some documents
    collection.find({"_id": ObjectId(id)}).toArray(function(err, docs) {
      assert.equal(err, null);
      callback(docs);
    });
}

const deleteDocumentsById = function(db, query, id, callback) {
    // Get the documents collection
    const collection = db.collection(query);
    // Find some documents
    collection.deleteOne({"_id": ObjectId(id)}, function(err, result) {
      assert.equal(err, null);
      callback(result);
    });
}

const insertDocuments = function(db, query, data, callback) {
    const collection = db.collection(query);
    // Insert some documents
    collection.insertMany([
        data
    ], function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
}

const updateDocumentsById = function(db, query, id, data, callback) {
    // Get the documents collection
    const collection = db.collection(query);
    // Find some documents
    collection.replaceOne({"_id": ObjectId(id)}, { $set: data }, function(err, result) {
        assert.equal(err, null);
        callback(result);
    });
}

Router.get('/send/message/:to', function(request, response){
    const to = request.params.to
    //console.log(clients.length)
    //console.log(clients[parseInt(to)])
    clients[to].emit('msjServer', {msj: 'alguien llamo users'})
    response.send('hola detalle')
})

App.use('/api', Router)

Server.listen(process.env.PORT, function(){
    console.log('mi primer node')
})



io.on('connection', function(socket){
    clients.push(socket)

    socket.emit('asignarCedula', {cedula: clients.length})

    console.log('alguien en el socket')
    socket.on('holaServer', function(data){
        console.log('me escribio un cliente',data.name)
    })

})